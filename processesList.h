#include <stdbool.h>
#include <sys/types.h>

typedef struct PNode PNode;

struct PNode {
	pid_t pid;
	char *command_line;
	PNode* next;
	PNode* prev;
};

typedef struct PQueue PQueue; /* Queue Data type for Processes List handling, implements FIFO */

struct PQueue {
	PNode *head;
	PNode *tail;
};


bool is_empty(PQueue* pqueue);
void enqueue_process(pid_t pid, char *command_line, PQueue* pqueue);
PNode* dequeue(PQueue* pqueue);
void delete_node(PNode* node, PQueue* pqueue);
