#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "processesList.h"

typedef struct PNode PNode;


typedef struct PQueue PQueue; /* Queue Data type for Processes List handling, implements FIFO */


bool is_empty(PQueue* pqueue);
void enqueue_process(pid_t pid, char *command_line, PQueue* pqueue);
PNode* dequeue(PQueue* pqueue);

/*
Enqueues process to the end of the queue
*/
void enqueue_process(pid_t pid, char *command_line, PQueue* pqueue) {
	PNode* node = (PNode*) malloc(sizeof(PNode));
	node->pid = pid;
	node->command_line = strdup(command_line);
	node->next = NULL;
	node->prev = pqueue->tail;
	if (is_empty(pqueue)) {
		pqueue->head = node;
		pqueue->tail = node;
	}
	else {
		pqueue->tail->next = node;
		pqueue->tail = node;
	}
}

/*
Dequeues process node from the head of the queue
*/
PNode* dequeue(PQueue* pqueue) {
	if (is_empty(pqueue))
		return NULL;
	PNode* node = pqueue->head;
	pqueue->head = pqueue->head->next;
	return node;
}

/*
Returns true(1)if the queue is empty, false(0) otherwise
*/
bool is_empty(PQueue* pqueue) {
	return pqueue->head == NULL;	
}

/*
Deletes the node PNode from the queue PQueue
*/
void delete_node(PNode* node, PQueue* pqueue) {
	if (node->prev != NULL) { //if deleting not from the head
		node->prev->next = node->next;
	} else {
		pqueue->head = node->next;
	}
	if (node->next != NULL) { //if deleting not from the tail
		node->next->prev = node->prev;
	} else {
		pqueue->tail = node->prev;
	}
	free(node);
}
