# Flush

A simple unix shell with minimal functionality that is nevertheless doing some useful things.


## Basic Functionality

flush prompts the user for input by printing the current working directory followed by a colon (```:```) character.
The entered text is split into command name and arguments. Arguments are separated from each other and from the command
name by space (ASCII 0x20) or tab (0x09) characters.

Parameters with quotes or backslash to escape whitespace characters (e.g. ```ls /my/home\ dir``` or ```ls "/my/home dir"```) are not handled. 
Flush waits for the termination of the started (foreground) process and prints the exit status of the command along with the command line like this:

```
/home/user/shelldev: /bin/echo test
test
Exit status [/bin/echo test] = 0
```

After printing the status, the shell is ready to accept a new command.
flush terminates when control-D (ASCII 0x04) is entered on the command line.

## Changing directories

The cd command to change directories (is implemented. A cd command without a parameter changes the path to the user’s home directory.

## I/O redirection
Simple I/O redirection is implemented, when this is indicated on the command line using the ```<``` or ```> ``` characters followed by a file name.
Input and output redirections is only accepted as the last parameters of a command line.
(excluding the “&” character for background processes, see below), e.g.:
```
/home/user/shell: ls > /tmp/foo
Exit status: 0
/home/user/shell: head -1 < /etc/passwd
root:*:0:0:System Administrator:/root:/bin/sh
Exit status: 0
/home/user/shell: head -1 < /etc/passwd > /tmp/foo2
Exit status: 0
```

## Background tasks
If a command line ends with the character ```&```, the entered command is executed as a background process, i.e.,
flush does not wait for the termination of the process but directly prompts the user to input a new command.
Before printing a new prompt, flush collects all background processes that have terminated (zombies) and prints
their exit status.

## Background task status
```jobs``` command prints all running background processes (PID and command line, one line per process).


## Pipelines

Pipelines are implemented with usual unix syntax, e.g.

```
/home/user/shell: ls -l | grep root | cat > /tmp/file

```
