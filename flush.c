#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "processesList.h"


void error(char* error_text);
void collect_print_and_delete_zombies(PQueue* pqueue);
void print_running_bg_processes(PQueue* pqueue);

int main(int argc, char *argv[]) {

    char cwd[PATH_MAX];
    size_t inputsize = 256;
    char *input = (char *)malloc(inputsize * sizeof(char)); //allocating memory for input from the user
    char *input_copy = (char *)malloc(inputsize * sizeof(char));
    char *delimeter = " \t\n";
    char *command, *token;
    char *params[32]; //array for storing parameters inputed by user
    int pipe_indxs[16];
    pid_t pid; //process id
    int status, outputfd, inputfd, output_ind, input_ind;
    bool bg; //true if task is background task
    PQueue* pqueue = (PQueue*) malloc(sizeof(PQueue*));

    while(1) {
    	//collect all background processes that have terminated (zombies) and print their exit status
    	 collect_print_and_delete_zombies(pqueue);

        //getting current path
        getcwd(cwd, sizeof(cwd));
        printf("\033[0;36m%s:\033[0;37m", cwd); //printing current directory in cyan, and changing back to white

        if (getline(&input,&inputsize,stdin) == -1) {//getting the input from the user
            printf("Exiting shell ..\n");
            exit(0);
        }

        int len = strlen(input);
        if (len > 0 && input[len-1] == '\n')
            input[len-1] = '\0';
        strcpy(input_copy, input); //coping input for futher output of exit code to the user
        len = strlen(input);
        bg = false;
        if (len > 0 && input[len-1] == '&') {//check if task is bg task
            input[len-1] = '\0';
            bg = true;
        }
        command = strtok(input, delimeter);
        int i = 0;
        params[i] = command; //first paramenter is the command itself
        i = 1;
        output_ind = -1; //variables for output and input redirection, store the index of >/< in params array, -1 if there is no i/o redirection
        input_ind = -1;
        int pipelines = 0; //variable for storing the number of | pipelines
        pipe_indxs[0] = -1; //array for storing indexes of "|" in params array, here first one is -1, to generalize, bcs we will need to +1 to get args for child processes
        while (token = strtok(NULL, delimeter)) {
            params[i] = token;
            if (strcmp(token, ">") == 0)
            	output_ind = i;
            if (strcmp(token, "<") == 0)
            	input_ind = i;
            if (strcmp(token, "|") == 0) {
              pipelines += 1;
              pipe_indxs[pipelines] = i;
            }
            i += 1;
        }
        params[i] = NULL;
        if  (strcmp(command, "cd") == 0 ) {
                if (i == 1) { //cd command without parameters
                    chdir(getenv("HOME")); //go to home directory if it is set, if not, just ignore
                }
                else if (i != 2) {
                    error("Wrong input\n");
                }
                else if (chdir(params[1]) != 0)
                    error("No such directory\n");
            }

         else if  (strcmp(command, "jobs") == 0 ) {
                if (i == 1)  //jobs command without parameters
                    print_running_bg_processes(pqueue);
                else
                    error("Wrong input\n");
            }
        else {
            //general case for pipelines or no pipelines
            int fd[pipelines][2]; //we need #pipelines pipes in case pipelining
            int j;
            // i is minimum of i, input ind, output ind
            if (input_ind > 0 &&  input_ind < i)
              i = input_ind;
            if (output_ind > 0 && output_ind  < i)
              i = output_ind;

            if (pipelines > 0) {
              for (j = 0; j < pipelines; j++) {
                if (pipe(fd[j]) < 0) //creating #pipelines pipes, so that processes in pipeline can communicate throught them
                  return 2;
                }
              }
              int pids[pipelines+1]; //pids of child processes, in case no pipelining, array with one element
              for (j = 0; j < pipelines+1; j++) {
                pids[j] = fork();
                if (pids[j] < 0)
                  return 1;
                if (pids[j] == 0) { //j-th child process in the pipeline
                  for (int v=0; v < pipelines; v++) {
                    if (v != j-1)
                      close(fd[v][0]); //fd[j-1][0] read end of previous pipe remains open, so that process reads from it
                    if (v != j)
                      close(fd[v][1]); //fd[j][1] write end of the pipe remains open, so that process writes to it, and next process will read from the read end of that pipe
                  }
                  //partitioning params into args of processes in the pipe using indexes of "|"
                  int start_indx = pipe_indxs[j]+1;
                  int end_indx;
                  if (j < pipelines)
                    end_indx = pipe_indxs[j+1] - 1; //inclusive
                  else //last process
                    end_indx = i-1; //i is index of last, before < , > evt.

                  char *cargs[end_indx - start_indx+2];
                  for (int o = start_indx; o <= end_indx; o++ ) {
                    cargs[o-start_indx] = params[o];
                  }
                  cargs[end_indx - start_indx+1] = NULL;
                  if (j < pipelines) {//if not the last process in the pipeline
                    dup2(fd[j][1], 1); //redirect sttdout to write end of j th pipe
                  } else {
                    // if there is output redirection
                    if (output_ind > 0)  {//command with params is before >
                  		outputfd = open(params[output_ind+1], O_WRONLY | O_CREAT, 0666);//0666 is an octal number for permission bits for owner, group and others, 0 means it is a file. 6 = (110). three perwmission bits, wrx correspondinly
                  		close(1); //close stdout
                  		dup2(outputfd, 1);
                  	}
                  }
                  close(fd[j][1]);
                  if (j > 0) {//if not the first process in the pipeline
                    dup2(fd[j-1][0], 0); //redirect stdin, read from the j-1 th pipe
                  }
                  else {
                    //check if thereis input redirection
                  	if (input_ind > 0)  {//command with params is before <
                  		inputfd = open(params[input_ind+1], O_RDONLY);
                  		close(0); //close stdin
                  		dup2(inputfd, 0);
                  	}
                  }
                  close(fd[j-1][0]);

                  if (execvp(cargs[0], cargs) < 0)
                    exit(EXIT_FAILURE);
                }


              }

              //closing all the pipes in parent process
                for (int v=0; v < pipelines; v++) {
                    close(fd[v][0]);
                    close(fd[v][1]);
                }

                if (!bg) { //if task is not background task, parent process waits until its done
                  for (int v=0; v < pipelines+1; v++) { //wait for all children
                    waitpid(pids[v], &status, 0); //status of the lst process will be saved eventually in status
                  }
                  	printf("Exit status [%s] = %d\n", input_copy, status);
                }
                else { //store process in a queue
                  for (int v=0; v < pipelines+1; v++) { //wait for all children
                    enqueue_process(pids[v], input_copy, pqueue); //in case pipelining, clearthe whole pipeline command is stored for each process in pipeline
                  }
                }
        }
    }

    return 0;
}

//prints the error text in red color
void error(char* error_text) {
    printf("\033[0;31m%s\033[0;37m", error_text);
}

void collect_print_and_delete_zombies(PQueue* pqueue) {
	PNode* node = pqueue->head;
	int status_child;
    	while (node != NULL) {
    		if (waitpid(node->pid, &status_child, WNOHANG) > 0) {//if child exited
    			printf("Process %d, started with command %s, terminated with status: : %d\n", node->pid, node->command_line, status_child);
    			PNode* next_copy = node->next;
    			delete_node(node, pqueue);
    			node = next_copy;
    		}
    		else
 			node = node->next;
    	}

}

void print_running_bg_processes(PQueue* pqueue) {
	printf("Running bg processes:\n");
	PNode* node = pqueue->head;
    	while (node != NULL) {
    		printf("%d: %s\n", node->pid, node->command_line);
 		node = node->next;
    	}

}
